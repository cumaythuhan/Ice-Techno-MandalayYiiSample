<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CompanydataSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="companydata-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'NO') ?>

    <?= $form->field($model, 'CompanyName') ?>

    <?= $form->field($model, 'Address') ?>

    <?= $form->field($model, 'PhoneNumber') ?>

    <?= $form->field($model, 'Email') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        
    </div>

    <?php ActiveForm::end(); ?>

</div>
