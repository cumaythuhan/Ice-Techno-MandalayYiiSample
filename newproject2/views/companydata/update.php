<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Companydata */

$this->title = 'Update Companydata: ' . $model->NO;
$this->params['breadcrumbs'][] = ['label' => 'Companydatas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NO, 'url' => ['view', 'id' => $model->NO]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="companydata-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
