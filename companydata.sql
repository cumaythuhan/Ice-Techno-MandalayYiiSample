-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2018 at 03:05 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `company1`
--

-- --------------------------------------------------------

--
-- Table structure for table `companydata`
--

CREATE TABLE `companydata` (
  `NO` int(5) NOT NULL,
  `CompanyName` varchar(255) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `PhoneNumber` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `companydata`
--

INSERT INTO `companydata` (`NO`, `CompanyName`, `Address`, `PhoneNumber`, `Email`) VALUES
(1, 'Royal Oasis', 'No.38,27th St,Bet 72nd &73rd St,Mandalay', '(02)35113', 'www.royaloasisburma.com'),
(2, 'Avamin', 'No.425,81st St,Bet 32nd &33rd St,Mandalay', '(02)39725', 'www.avaminmyanmar.com'),
(3, 'chin Taung Tan', 'No.2,Shed-1,Plot=195,Thiri Mandalar Car Compound,Apyin Tan,Mandalay', '(02)092450355', 'www.chintaungtan.com'),
(4, 'Polygold Co,.Ltd.', 'E/8,36th Street, Between 60th and 61th Stret,Mandalay', '09798675474', 'www.polygoldmm.com'),
(5, 'One $ Only Fine Dlilng', 'No.29,26th Street,Bet 63rd $ 64th Street,Mandalay', '09 257170007', 'www.facebook.com/OneAndOnlyfineDining.Myanmar'),
(6, 'Win Sabei Trading', '27st,bet82$83 StreetMandalay', '09 420115887', 'www.winsabeitrading.com'),
(7, 'LM Travel Myanmar', 'Flat no 25/room 31, myayinandar housing,chanmyarharsi tsp,Mandalay', '09 421723682', 'www.lmtravelmyanmar.com'),
(8, 'Min Naung Khant Co.,Ltd', 'Na-9/2,42nd St,Bet 62nd & 63rd St.Mahar Myaing Qt(2),Mahar Aung Myay Township,Mandalay', '09 420039333', 'www.minnaingkhant,com'),
(9, 'GENIUS', '86th Street,Bet 15th &16th Street,Aung Myae That San,Mandalay', '09 793444401', ''),
(10, 'Pyin Oo Lwin Real Estate', 'No.135,Hnee Pagoda Rd,Pyin Oo Lwin,Mandalay', '09 4316723', 'pyinoolwinrealestate.wordpress.com/'),
(11, 'Binary To Reality[IT Consulting & Web Solutions]', 'No 261,33rd Street,Bet 83rd &84th Street.Mandalay', '09 2020841', 'www.binarytoreality,com'),
(12, 'NEC(Naypyidaw Engineering Center)', '253/A,34th St.,Cor.of 84th St.,Mandalay', '09 2150200', 'www.necengineering.com'),
(13, 'Htilar(Than Mg & Brothers)', 'Top of Yaw Min Gyi St(West),Industrial Zone(1),Pyi Gyi Tagon,Mandalay', '09 975672178', 'wwww.thilarthanmg.com'),
(14, '7 Copy', '43th Street,BEt 72nd & 73rd Street,Mandalay', '09 2019846', 'www.7copycompany.com'),
(15, 'Leo', '9/2,42nd St,Bet 62nd &63rd St,Mandalay', '09 2038747', 'www.minnaingkhant.com'),
(16, 'Kimia International Pte.Ltd', '133 Cecil Street,#12=03,Keck Seng Tower,Bharma,Mandalay', '+65-6227-6365', 'www.kimiainternational.com/'),
(17, 'Mobile Mother', '26St, Bet76rd &77th Street,Mandalay', '02-72041', 'www.mobilemotherinternational.com'),
(18, 'Swe Tha Har Myanmar Co.,Ltd.', 'no,252/2,82nd St,Bet 23rd St &24th st,Mandalay', '02-23278', 'www.swetrhaharmm.com'),
(19, 'Central Computer', 'rm.b-1,40th St,Cor 73rd St,Mandalay', '09 444008562', 'www.centralmdy.com'),
(20, 'Dream House', 'No.232,33rd St,Bet 81st&82nd St,Mandalay', '02-24999', 'www.dreamhousemdy.com'),
(21, 'Eastern Photo Studio', '182,28th St,Bet 80th & 81st St,Mandalay', '02 73277', 'www.easternphotostudio.com'),
(22, 'Ayar Min', 'Ma-9/11,64th St,Bet Cherry St & Theikpan St,Mandalay', '02-80187', 'www.abgroup.com'),
(23, 'Forward', 'Pa-9/3,Bet 59th & 60th St,Mandalay', '02-62517', 'www.dvlotteryform.com'),
(24, 'Green Elephant', 'H-3,27th St,Bet 64th &65th St,Mandalay', '02-61237', 'www.greenelephant-restaurants.com'),
(25, 'Magha Ko Lwin', 'No.205,36th St,Bet 81st & 82nd St,Mandalay', '02-38713', 'www.maghakolwin.com'),
(26, 'Mann Shin Oxygen', 'Bastone(6),Yangon-Mandalay Rd(East),Industrial Zone,Mandalay', '025188448', 'www.facebook.com/mannshinoxygen/'),
(27, 'MCC', '35th,Bet 56th &57th St,Phayar Lane,Mandalay', '02-77930', 'www.mcc.com.com'),
(28, 'Myanmar C.P Livestock', 'G-4,Yangon-Mandalay Rd,Set Hmu(1),Mandalay', '02-5154127', 'www.myanmarcplivestock.com'),
(29, 'Shinning Star Tours Express Ltd', 'No.279,81st St,BEt 26th &27th St,CATZ,Mandalay', '02-36335', 'www.shiningstartours.com'),
(30, 'Soe Wint', 'No.10,78th Rd,Bet 33rd & 34th St,Mandalay', '02-34557', 'www.oakley.com'),
(31, 'Sweety Life', 'D-1/A,62nd St,Pan Khinn St,Industrisl Zone(1),Mandalay', '02-5153850', 'www.maykaung.com'),
(32, 'Thazin', '24,85th St,Bet 29th &30th St,Mandalay', '02-39602', 'www.mdythazinbis.com'),
(33, 'Yadanabone', '61st St,Bet 41st & 42nd St,Mahar Myaing(2),Mandalay', '02-31071', 'www.yadnapon.com'),
(34, 'Yangon Airways', 'Rm-3,78th St,Bet 29th &30th St,SY Bldg,Mandalay', '02-34405', 'www.yangonair.com'),
(35, 'United Pacific Co.,Ltd.', 'No.79,Oak Pon Seik St,Mayangone,Mandalay', '01-652888', 'wwww.unitedpacificmm.con'),
(36, 'ACD Gallary', 'Myatlay St,Kyansitthar Ward,Thamudiric Qtr,Bagan-nyaung Oo,Mandalay', '02-67025', 'www.acdmedia.com/creativedesignermedia.com'),
(37, 'ADK Advertising Group', 'Plot-801,65th St,BEt 26th & 27t St,Mandalay', '09 2039016', 'www.adkadveretosing.con'),
(38, 'Champion', 'No.6,26th St,Bet 86th & 87th St,Aung Myay Thar San,Mandalay', '09-2006970', 'www.luckychampion.com'),
(39, 'Diamond Rose Co.,Ltd', 'No.122,73rd St,bEt 29th & 39t St,Mandalay', '02-68674', 'www.trinricosteel.com'),
(40, 'Five Star', 'No.357,83rd St,Bet 32nd & 33rd St,Chan Aye Thar San,Mandalay', '02-24889', 'www.fivestarpipes.com.mm'),
(41, 'Forever', 'No.398/C,81st St,Bet 30t & 31st St,Mandalay', '02-35295', 'www.foreverweddingstudio.com'),
(42, 'Fuji', 'No.427,83nd Rd,Bet 32nd &33rd St,Chan Aye Thar Sna,Mandalay', '02-35429', 'www.fujijlcyanmar.com'),
(43, 'Golden Triangle', '85,84th St,,Bet 28th &29th St,Mandalay', '02-31421', 'www.gthtrading.com'),
(44, 'Hein Engineering Group', 'H-198 A,Ka Naung Min Thar Gyi St,Cor 61st St,Industrial Zone(1),Mandalay', '02-34824', 'www.heinengineering.com'),
(45, 'Heritage Sports & Golf', 'H-1.30th St,Bet 77th &78th St,Mandalay', '02-34918', 'www.heritagegolfing.com'),
(46, 'Innwa', 'No.179,83rd St,Bet 28t &29th St,Mandalay', '02-23072', 'www.innwabakery.com'),
(47, 'Kan Kaung', 'Rm-1,cor of 31st & 85th St,Aung Thiri Qtr,Mandalay', '02-35620', 'www.kankaung.com'),
(48, 'May & Mark Gems', '12/5,30th St,Bet 73rd & 74th St,Chan Aye Thar San,Mandalay', '02-33229', 'www.maynmark.com'),
(49, 'Nay Lin Aung', 'No.225,74th St.Bet 35th &36th St,Mandalay', '09 20003386', 'www.nla.accountamcyentre.com'),
(50, 'Peninsula Enterprises Ltd', 'No.134,26th(B)St,BEt 83rd & 84th St,Aung Myae Thar Zan,Mandalay', '02-36720', 'www.peninsulaasia.com'),
(51, 'Prime Boss Co.,Ltd', 'No.185,33rd St,Bet 76th & 77th St,Chan Aye Thar San,Mandalay', '09 5060317', 'www.primeboss.com.mm'),
(52, 'Star', 'No.90,30th St,Bet 73td &74th St,Infront of Institude of Medicine,Mandalay', '02-74095', 'www.star.ent.com'),
(53, 'Thet Paing Soe Co.,Ltd', 'No.9,26th St,Bet 78th &79th St,CATZ,Mandalay', '02-35412', 'www.royalmyintsoe.com'),
(54, 'Zarni', 'Bet 83rd & 84th St,Bet 34th & 35th St,West Sewing Compound,Mandalay', '09 2060044', 'www.zarnielect.com'),
(55, 'Agro Grain Co Ltd', 'No.295,Bo Sun Pet St,Pabedan,Mandalay', '09 402620504', 'www.agrograin.com'),
(56, 'Vakok', '40,Min Gyi St,Near Ma Nya Ka Bus Stop,Taung Thu Gone Ward,ISN,Mandalay', '09 2450595', 'www.vakok.com.mm'),
(57, 'ACC', '84th St,Bet 31st & 32nd St,Mandalay', '02 65566', 'www.acc.com.mm'),
(58, 'Blue Sky', '82nd Rd,Bet 14th & 15th St,Mandalay', '02 69587', 'www.bluesky.com.mm'),
(59, 'Carat', 'No-D/17th St,Bet 32nd & 33rd St,Mandalay', '09 2003430', 'www.carat.com.mm'),
(60, 'Daily', 'Cor of 35th & 70th St,Mandalay', '02 62980', 'www.daily.com.mm'),
(61, 'Elegant', 'No.97,27th St,Bet 75th & 76th St,Mandalay', '02 71315', 'www.elegant.com.mm'),
(62, 'Dumbo Cake', '80th St,Bet 28th &29th St,Mandalay', '02 31813', 'www.dumbocake.com.mm'),
(63, 'Future\'s Sky', '152,73rd St,Bet 33rd &34th St,Mandalay', '02 72958', 'www.futuresky.com'),
(64, 'G G', 'No.347,83rd Rd,Bet 32nd & 33rd St,Mandalay', '02 34404', 'www.ggtheinsoe.com'),
(65, 'Havana', 'No.259,33rd St,Bet 83rd &84st,Mandalay', '02 24498', 'www.havana.com.mm'),
(66, 'Home Tech', 'No.245,31st St,Bet 83rd & 84th St,Mandalay', '09 91037945', 'www.hometech.com.mm'),
(67, 'i-Trend', 'A-4,Theik Pan St,Bet 77th & 78th St,Mandalay', '09 2054744', 'www.itrend.com.mm'),
(68, 'Kyaw Swar Win', 'No.233,73rd St,Bet 37th & 38th St,Mandalay', '02 62921', 'www.kyawswarwin963.com'),
(69, 'Laurel', '78th St,Bet 31st&32nd St,Gold Plaza 2nd Flr,Mandalay', '09 5116433', 'www.laurel.com.mm'),
(70, 'Mann Thiri', 'No.133,29th St,Bet 81st &82nd St,Mandalay', '02 65828', 'www.mannthiri.com'),
(71, 'MK', '77th St,Bet 33rd &34th St,Mandalay', '02 73718', 'www.myokyaw456.com'),
(72, 'Naing Ko', '87th St,Cor 31st St,Mandalay', '092030795', 'www.naingko345.com'),
(73, 'New Star', '35th St,Bet 84th &85th St,Mandalay', '092021501', 'www.newstar.com'),
(74, 'OK 2000 Store', '19th St,Cor of 62th St,Mandalay', '09 2006293', 'www.ok2000store.com'),
(75, 'Oscar', 'No.324,81st St,Bet 26th &27th St,Mandalay', '02 24338', 'www.oscar123.com'),
(76, 'Page Master', '66th St,Cor 30th St,Mandalay', '02 61272', 'www.pagemaster.com'),
(77, 'Rose', '11st St,Bet 86th &87th St,Myat Shwe War Chan,Mandalay', '02 38424', 'www.rose789.com'),
(78, 'Safari', 'Plot-445,Than Hlet Hmaw(South),Thanat Pin Yat,Mandalay', '02 34011', 'www.safari567.com.mm'),
(79, 'Three Stars Co.,Ltd', 'No.1,30th St,Bet 62nd &63rd St,Mandalay', '02 61198', 'www.threestars345.com'),
(80, 'Tet Lu', 'No.5,Phone Taw Toe 1st St,Bet 65th &66th St,Plot-41,Mandalay', '02 60660', 'www.tetlu.com'),
(81, 'UMW Engineering Services Ltd', 'Ma-9/3,Cor Of 64nd St,Chan Aye Thar Zan,Mandalay', '02 80663', 'www.umwengineerintservices.com'),
(82, 'Venda', 'No(G-22),82nd St,Bet 26th &27th St,Mandalay', '02 32753', 'www.venda.com'),
(83, 'Z', '32nd St,Bet 76th &77th St,Chan Aye Thar San,Mandalay', '02 33833', 'www.zawminmaung.com'),
(84, 'ZBT CO.,Ltd', 'Rm-D-1,78th St,Bet 27th &82nd St,Mandalay', '0271789', 'www.zinbotun.com'),
(85, 'Yupar', 'W-16,70th St,Bet 29th &30th St,Mandalay', '0274253', 'www.yupar.com'),
(86, 'Y.C.C', 'Bet 15th St &16th St,Bet 87th & 88th St,Mandalay', '092030366', 'www.yanchanchan.com'),
(87, 'Winder', 'Cor Of 25th & 83th St,Mandalay', '09 9060134', 'www.winder.com'),
(88, 'US', 'No.221,34th St, Bet 83rd & 84th St,Mandalay', '02 65062', 'www.usan.com'),
(89, 'Valentine', 'N0.259,35th St,Bet 80th &81st St,Mandalay', '096802223', 'www.valentine.com'),
(90, 'Victoria', '76th St,Bet 34th &35th St,Mandalay', '09 2004957', 'www.victora.com'),
(91, 'United Paints Co.,Ltd', 'Lagyi-9/1,Salane,62nd St,Gange Qtz,Bet Kyan Sitthar St & Zawgi St,Mandalay', '02 80811', 'www.unitedpaints.com'),
(92, 'Twilight Travels & Tours Co.,Ltd', 'N0.A.3G,77th St,Bet 31st & 32nd St,Mandalay', '02 34665', 'www.twilighttravelsandtours.com'),
(93, 'U Aung', 'No.79/G Flr,Zay Cho,Mandalay', '02 35199', 'www.uaung.com'),
(94, 'SY', '77th St,Bet 30th & 31th St,Mandalay', '02 35541', 'www.sawyan.com'),
(95, 'Sweet Smile', 'Cor Of 30th St & 65th St,Mandalay', '09 91007771', 'www.sweetsmile.c0m'),
(96, 'Sinma Furnishings Co.,Ltd', '84th St,Bet 32nd & 33rd St,Mandalay', '0991013303', 'www.sinmafurnishings.com'),
(97, 'Siam Star Co.,Ltd', 'Bldg-8,Rm-3,35th St,Bet 82nd & 83rd St,Mandalay', '02 22704', 'www.siamstar.com'),
(98, 'Seven Star Co.,Ltd', 'No.269,82nd St,Bet 27th &28th St,Mandalay', '02 60994', 'www.sevenstar.com'),
(99, 'Ruby Dragon Companies', 'N0.417,58th St,Bet 22nd & 23rd St,Mandalay', '02 61582', 'www.rubydragoncompanies.com'),
(100, 'Ryuji International Co Ltd', '5,39th St,Bet 78th & 79th St,Mandalay', '02 63207', 'www,ryujiinternational.com'),
(101, 'AAA', 'AAA', '12345', '12345@gmail.com'),
(102, '', '', '', ''),
(103, '', '', '', ''),
(104, 'AAB', 'AAA', '12345', '123456@gmail.com'),
(105, 'AAAAC', 'AAA', '12345', '12345@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companydata`
--
ALTER TABLE `companydata`
  ADD PRIMARY KEY (`NO`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companydata`
--
ALTER TABLE `companydata`
  MODIFY `NO` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
